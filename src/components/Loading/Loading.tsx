import React, { Component } from "react";
import { Spinner } from "react-bootstrap";

interface Props {
  animation?: "border" | "grow";
  variant?:
    | "primary"
    | "secondary"
    | "success"
    | "danger"
    | "warning"
    | "info"
    | "light"
    | "dark";
  text?: string;
  textClass?: string;
}

export class Loading extends Component<Props> {
  render = () => {
    const variant = this.props.variant || "primary";
    const animation = this.props.animation || "border";
    const { text, textClass } = this.props;
    return (
      <div className="w-100 text-center p-3">
        <Spinner animation={animation} variant={variant} />
        {text && <h5 className={textClass}>{text}</h5>}
      </div>
    );
  };
}
