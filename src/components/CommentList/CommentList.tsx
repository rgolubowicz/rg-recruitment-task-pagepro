import React, { Component } from "react";
import { Comment } from "../../models";
import { ListGroup, Button } from "react-bootstrap";
import "./CommentList.scss";
import { Loading } from "../Loading";
import { CommentSmall } from "../CommentSmall";
import CommentModal from "../modals/CommentModal";

interface Props {
  comments: Comment[];
  isFetching: boolean;
  allItems: number;
  take: number;
  visible: boolean;
  match?: any;
  newCommentModalVisible: boolean;
  newCommentError: string;
  newCommentSubmitting: boolean;
  loadMorePostComments: (take: number) => void;
  toggleComments: () => void;
  showNewCommentModal: () => void;
  closeNewCommentModal: () => void;
  pushPostComment: (email: string, title: string, body: string) => void;
}

export class CommentList extends Component<Props> {
  loadMore = () => {
    this.props.loadMorePostComments(5);
  };

  render = () => {
    const {
      comments,
      isFetching,
      take,
      allItems,
      visible,
      toggleComments,
      showNewCommentModal,
      newCommentModalVisible,
      closeNewCommentModal,
      pushPostComment,
      newCommentSubmitting,
      newCommentError
    } = this.props;

    let loadMoreBlock, commentListEmptyBlock;

    if (take < allItems && !isFetching) {
      loadMoreBlock = <Button onClick={this.loadMore}>Load more</Button>;
    }

    if (!isFetching && comments.length === 0) {
      commentListEmptyBlock = <h3>Comment list is empty</h3>;
    }

    return (
      <div className="py-4">
        <div className="d-flex justify-content-between">
          <small
            className="btn-link c-pointer text-info"
            onClick={toggleComments}
          >
            {visible ? "Hide" : "Show"} comments
          </small>
          {visible && (
            <small className="btn-link c-pointer" onClick={showNewCommentModal}>
              Add comment
            </small>
          )}
        </div>
        {visible && (
          <div className="test51">
            <ListGroup>
              {comments.map(comment => (
                <ListGroup.Item className="rounded-0 mb-3" key={comment.id}>
                  <CommentSmall {...comment} />
                </ListGroup.Item>
              ))}
            </ListGroup>

            {commentListEmptyBlock}
            {isFetching && <Loading text="Loading comments" />}
            {loadMoreBlock}
          </div>
        )}
        <CommentModal
          visible={newCommentModalVisible}
          onClose={closeNewCommentModal}
          onSubmit={pushPostComment}
          adding={newCommentSubmitting}
          error={newCommentError}
        />
      </div>
    );
  };
}
