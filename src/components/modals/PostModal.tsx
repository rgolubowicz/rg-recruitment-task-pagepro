import React, { Component } from "react";
import { Modal, Button, Alert } from "react-bootstrap";
import { renderField, required, renderTextarea } from "../../helpers/form";
import { Field, reduxForm } from "redux-form";

interface Props {
  visible: boolean;
  onClose: () => void;
  onSubmit: (title: string, body: string) => void;
  handleSubmit: any;
  pristine: boolean;
  reset: any;
  adding: boolean;
  error: string;
}

class PostModal extends Component<Props> {
  handleSubmit = ({ title, body }) => {
    this.props.onSubmit(title, body);
  };

  render = () => {
    const {
      visible,
      onClose,
      adding,
      handleSubmit,
      error,
      pristine
    } = this.props;
    return (
      <Modal show={visible} onHide={onClose} centered>
        <Modal.Header closeButton={!adding}>
          <Modal.Title>Add post</Modal.Title>
        </Modal.Header>
        <form onSubmit={handleSubmit(this.handleSubmit)}>
          <Modal.Body>
            {error && (
              <Alert variant="danger">
                An error occurred while adding the post
              </Alert>
            )}
            <Field
              name="title"
              type="text"
              component={renderField}
              label="Title"
              validate={[required]}
            />
            <Field
              name="body"
              type="text"
              component={renderTextarea}
              label="Body"
              validate={[required]}
            />
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="secondary"
              className="px-4"
              onClick={onClose}
              disabled={adding}
            >
              Cancel
            </Button>
            <Button
              type="submit"
              className="px-4"
              variant="primary"
              disabled={pristine || adding}
            >
              {adding ? "Saving..." : "Save"}
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  };
}

export default reduxForm({
  form: "newPostForm"
})(PostModal);
