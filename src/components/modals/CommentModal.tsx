import React, { Component } from "react";
import { Modal, Button, Alert } from "react-bootstrap";
import {
  renderField,
  required,
  renderTextarea,
  email
} from "../../helpers/form";
import { Field, reduxForm } from "redux-form";

interface Props {
  visible: boolean;
  onClose: () => void;
  onSubmit: (email: string, title: string, body: string) => void;
  handleSubmit: any;
  pristine: boolean;
  reset: any;
  adding: boolean;
  error: string;
}

class CommentModal extends Component<Props> {
  handleSubmit = ({ email, name, body }) => {
    this.props.onSubmit(email, name, body);
  };

  render = () => {
    const {
      visible,
      onClose,
      adding,
      error,
      pristine,
      handleSubmit
    } = this.props;
    return (
      <Modal show={visible} onHide={onClose} centered>
        <Modal.Header closeButton={!adding}>
          <Modal.Title>Add comment</Modal.Title>
        </Modal.Header>
        <form onSubmit={handleSubmit(this.handleSubmit)}>
          <Modal.Body>
            {error && (
              <Alert variant="danger">
                An error occurred while adding the post
              </Alert>
            )}
            <Field
              name="name"
              type="text"
              component={renderField}
              label="Name"
              validate={[required]}
            />
            <Field
              name="email"
              type="email"
              component={renderField}
              label="Email"
              validate={[required, email]}
            />
            <Field
              name="body"
              type="text"
              component={renderTextarea}
              label="Body"
              validate={[required]}
            />
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="secondary"
              className="px-4"
              onClick={onClose}
              disabled={adding}
            >
              Cancel
            </Button>
            <Button
              type="submit"
              className="px-4"
              variant="primary"
              disabled={pristine || adding}
            >
              {adding ? "Saving..." : "Save"}
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  };
}

export default reduxForm({
  form: "newPostCommentForm"
})(CommentModal);
