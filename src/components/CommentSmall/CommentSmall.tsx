import React, { Component } from "react";
import { Comment } from "../../models";

interface Props extends Comment {}
export class CommentSmall extends Component<Props> {
  render = () => {
    const { name, email, body } = this.props;
    return (
      <div>
        <div className="d-flex justify-content-between mb-2">
          <small className="font-weight-bold mb-0">{name}</small>
          <a href={"mailto:" + email} className="mb-0 small ml-3">
            {email}
          </a>
        </div>
        <p className="mb-0">{body}</p>
      </div>
    );
  };
}
