import React, { Component } from "react";
import { Post } from "../../models";
import { ListGroup, Button } from "react-bootstrap";
import "./PostList.scss";
import { PostSmall } from "../PostSmall/PostSmall";
import { Link } from "react-router-dom";
import { Loading } from "../Loading";
import { FaPlusCircle } from "react-icons/fa";
import PostModal from "../modals/PostModal";

interface Props {
  posts: Post[];
  isFetching: boolean;
  allItems: number;
  take: number;
  newPostModalVisible: boolean;
  newPostError: string;
  newPostSubmitting: boolean;
  userLoaded: boolean;
  match?: any;
  removePost: (postId: number) => void;
  pushPost: (title: string, body: string) => void;
  loadMorePosts: (take: number) => void;
  showNewPostModal: () => void;
  closeNewPostModal: () => void;
}

export class PostList extends Component<Props> {
  loadMore = () => {
    this.props.loadMorePosts(5);
  };

  render = () => {
    const {
      userLoaded,
      posts,
      isFetching,
      take,
      allItems,
      showNewPostModal,
      match,
      removePost,
      newPostModalVisible,
      closeNewPostModal,
      pushPost,
      newPostSubmitting,
      newPostError
    } = this.props;

    if (!userLoaded) {
      return null;
    }

    let loadMoreBlock;

    if (take < allItems && !isFetching) {
      loadMoreBlock = <Button onClick={this.loadMore}>Load more</Button>;
    }

    return (
      <div className="py-4">
        <div className="d-flex justify-content-between">
          <h3 className="d-inline-block mb-0">Posts</h3>
          <h5
            className="d-inline-block post__new--button c-pointer mb-0"
            onClick={showNewPostModal}
          >
            <FaPlusCircle />
          </h5>
        </div>
        <ListGroup>
          {posts.map(post => (
            <Link
              to={`${match.url}/${post.id}`}
              className="link-unstyled mb-3"
              key={post.id}
            >
              <ListGroup.Item action className="rounded-0">
                <PostSmall {...post} deletePost={removePost} />
              </ListGroup.Item>
            </Link>
          ))}
        </ListGroup>
        {isFetching && <Loading text="Loading posts" />}
        {loadMoreBlock}
        <PostModal
          visible={newPostModalVisible}
          onClose={closeNewPostModal}
          onSubmit={pushPost}
          adding={newPostSubmitting}
          error={newPostError}
        />
      </div>
    );
  };
}
