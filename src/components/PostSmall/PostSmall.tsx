import React, { Component } from "react";
import { FaTrashAlt, FaArrowRight } from "react-icons/fa";
import "./PostSmall.scss";
import { Post } from "../../models";

interface Props extends Post {
  deletePost: (id: number) => void;
}

export class PostSmall extends Component<Props> {
  onDelete = (event): void => {
    event.stopPropagation();
    event.preventDefault();
    this.props.deletePost(this.props.id);
  };

  render = () => {
    const { title } = this.props;
    return (
      <div className="h6 d-flex align-items-center mb-0">
        <h4 className="mb-0 trash__icon" onClick={this.onDelete}>
          <FaTrashAlt />
        </h4>
        <span className="mx-4 post__title">{title}</span>
        <h4 className="ml-auto mb-0">
          <FaArrowRight />
        </h4>
      </div>
    );
  };
}
