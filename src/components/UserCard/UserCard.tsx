import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import { User } from "../../models";
import { Link } from "react-router-dom";

interface Props extends User {}

export class UserCard extends Component<Props> {
  render = () => {
    const { id, name, email, phone, website, company } = this.props;
    return (
      <Card className="h-100">
        <Card.Body>
          <Card.Title className="text-center">{name}</Card.Title>
          <div className="mb-0">
            <a href={"mailto: " + email}>{email}</a>
          </div>
          <p className="mb-0">
            <a href={"tel: " + phone}>{phone}</a>
          </p>
          <p>
            <a target="_blank" rel="noopener noreferrer" href={"//" + website}>
              {website}
            </a>
          </p>
          {company && (
            <div>
              <p className="mb-0 font-weight-bold">{company.name}</p>
              <p className="mb-0">{company.catchPhrase}</p>
              <p className="font-weight-bold">{company.bs}</p>
            </div>
          )}
        </Card.Body>
        <Card.Footer>
          <Link to={`users/${id}`} className="link-unstyled">
            <Button variant="outline-info" block>
              Details
            </Button>
          </Link>
        </Card.Footer>
      </Card>
    );
  };
}
