import { FETCH_USERS_REQUESTED, FETCH_USERS_CANCELED, FETCH_USERS_DONE, FETCH_USERS_FAILED } from '../action-types';
import { User } from '../../models';

export const getUsers = () => (dispatch, getState) => {
    if (getState().users.items.length === 0) {
        dispatch(getUsersCanceledAction());
        dispatch(getUsersAction());
    }
};

const getUsersAction = () => ({
    type: FETCH_USERS_REQUESTED
});

export const getUsersCanceled = () => dispatch => {
    dispatch(getUsersCanceledAction());
};

const getUsersCanceledAction = () => ({
    type: FETCH_USERS_CANCELED
});

export const getUsersDone = (users: User[]) => dispatch => {
    dispatch(getUsersDoneAction(users));
};

const getUsersDoneAction = (users: User[]) => ({
    type: FETCH_USERS_DONE,
    payload: users
});

export const getUsersFailed = error => dispatch => {
    dispatch(getUsersFailedAction(error));
};

const getUsersFailedAction = error => ({
    type: FETCH_USERS_FAILED,
    payload: error
});