import {
    FETCH_POST_REQUESTED,
    FETCH_POST_CANCELED,
    FETCH_POST_DONE,
    FETCH_POST_FAILED,
    FETCH_POST_COMMENTS_CANCELED,
    FETCH_POST_COMMENTS_DONE,
    FETCH_POST_COMMENTS_FAILED,
    FETCH_POST_COMMENTS_REQUESTED,
    LOAD_MORE_POST_COMMENTS,
    TOGGLE_POST_COMMENTS,
    CHANGE_NEW_COMMENT_MODAL_VISIBILITY,
    PUSH_POST_COMMENT_REQUESTED,
    PUSH_POST_COMMENT_DONE,
    PUSH_POST_COMMENT_FAILED,
    RESET_POST_COMMENTS
} from '../action-types';
import { Post, Comment } from '../../models';
import { getById } from '../selectors/getById';
import { reset } from 'redux-form';

/* POST ACTIONS */

export const getPost = (postId: number) => (dispatch, getState) => {
    const { post, user } = getState();
    const savedPost = getById(user.posts.items, postId);
    if (savedPost != null) {
        dispatch(getPostDoneAction(savedPost));
        if (post.data == null || postId !== post.data.id) {
            dispatch(resetPostCommentsAction());
        }
    } else {
        if (post.data == null || post.data.id !== postId) {
            dispatch(getPostCanceledAction());
            dispatch(getPostAction(postId));
        }
    }
};

const getPostAction = postId => ({
    type: FETCH_POST_REQUESTED,
    payload: postId
});

const resetPostCommentsAction = () => ({
    type: RESET_POST_COMMENTS
})

export const getPostCanceled = () => dispatch => {
    dispatch(getPostCanceledAction());
};

const getPostCanceledAction = () => ({
    type: FETCH_POST_CANCELED
});

export const getPostDone = (post: Post) => dispatch => {
    dispatch(getPostDoneAction(post));
};

const getPostDoneAction = (post: Post) => ({
    type: FETCH_POST_DONE,
    payload: post
});

export const getPostFailed = error => dispatch => {
    dispatch(getPostFailedAction(error));
};

const getPostFailedAction = error => ({
    type: FETCH_POST_FAILED,
    payload: error
});


/* POST COMMENTS ACTIONS */

export const getPostComments = (postId: number) => (dispatch, getState) => {
    const { data, comments } = getState().post;
    if (data == null || data.id !== postId || comments.items.length === 0) {
        dispatch(getPostCommentsCanceledAction());
        dispatch(getPostCommentsAction(postId));
    }
};

const getPostCommentsAction = postId => ({
    type: FETCH_POST_COMMENTS_REQUESTED,
    payload: postId
});


export const getPostCommentsCanceled = () => dispatch => {
    dispatch(getPostCommentsCanceledAction());
};

const getPostCommentsCanceledAction = () => ({
    type: FETCH_POST_COMMENTS_CANCELED
});

export const getPostCommentsDone = (comments: Comment[]) => dispatch => {
    dispatch(getPostCommentsDoneAction(comments));
};

const getPostCommentsDoneAction = (comments: Comment[]) => ({
    type: FETCH_POST_COMMENTS_DONE,
    payload: comments
});

export const getPostCommentsFailed = error => dispatch => {
    dispatch(getPostCommentsFailedAction(error));
};

const getPostCommentsFailedAction = error => ({
    type: FETCH_POST_COMMENTS_FAILED,
    payload: error
});

export const loadMorePostComments = (take: number = 10) => (dispatch, getState) => {
    const { comments } = getState().post;
    dispatch(loadMorePostsAction(comments.take + take));
};

const loadMorePostsAction = (take: number) => ({
    type: LOAD_MORE_POST_COMMENTS,
    payload: take
});

export const togglePostComments = () => (dispatch, getState) => {
    const { data, comments } = getState().post;
    dispatch(getPostComments(data.id));
    dispatch(togglePostCommentsAction(!comments.visible));
};

const togglePostCommentsAction = (visible: boolean) => ({
    type: TOGGLE_POST_COMMENTS,
    payload: visible
})

export const closeNewCommentModal = () => dispatch => {
    dispatch(changeNewCommentModalVisibility(false));
}

export const showNewCommentModal = () => dispatch => {
    dispatch(changeNewCommentModalVisibility(true));
}

const changeNewCommentModalVisibility = (visible: boolean) => ({
    type: CHANGE_NEW_COMMENT_MODAL_VISIBILITY,
    payload: visible
});

export const pushPostComment = (email: string, title: string, body: string) => (dispatch, getState) => {
    const { data } = getState().post;
    dispatch(pushPostCommentAction(data.id, email, title, body));
}

const pushPostCommentAction = (postId: number, email: string, title: string, body: string) => ({
    type: PUSH_POST_COMMENT_REQUESTED,
    payload: { postId, email, title, body }
});

export const pushPostCommentDone = (comment: Comment) => dispatch => {
    dispatch(pushPostCommentDoneAction(comment));
    dispatch(reset('newPostCommentForm'));
}

const pushPostCommentDoneAction = (comment: Comment) => ({
    type: PUSH_POST_COMMENT_DONE,
    payload: comment
});

export const pushPostCommentFailed = (error) => dispatch => {
    dispatch(pushPostCommentFailedAction(error));
}

const pushPostCommentFailedAction = (error: string) => ({
    type: PUSH_POST_COMMENT_FAILED,
    payload: error
});