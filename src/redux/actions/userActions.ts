import {
    FETCH_USER_REQUESTED,
    FETCH_USER_CANCELED,
    FETCH_USER_DONE,
    FETCH_USER_FAILED,
    FETCH_USER_POSTS_FAILED,
    FETCH_USER_POSTS_REQUESTED,
    FETCH_USER_POSTS_CANCELED,
    FETCH_USER_POSTS_DONE,
    REMOVE_USER_POST_REQUESTED,
    REMOVE_USER_POST_FAILED,
    UPDATE_USER_POSTS,
    LOAD_MORE_POSTS,
    CHANGE_NEW_POST_MODAL_VISIBILITY,
    PUSH_USER_POST_REQUESTED,
    PUSH_USER_POST_FAILED,
    PUSH_USER_POST_DONE
} from '../action-types';
import { User, Post } from '../../models';
import { reset } from 'redux-form';

/* USER DETAILS ACTIONS */

export const getUser = (userId: number) => (dispatch, getState) => {
    const { data } = getState().user;
    if (data == null || data.id !== userId) {
        dispatch(getUserCanceledAction());
        dispatch(getUserAction(userId));
    }
};

const getUserAction = userId => ({
    type: FETCH_USER_REQUESTED,
    payload: userId
});

export const getUserCanceled = () => dispatch => {
    dispatch(getUserCanceledAction());
};

const getUserCanceledAction = () => ({
    type: FETCH_USER_CANCELED
});

export const getUserDone = (user: User) => dispatch => {
    dispatch(getUserDoneAction(user));
};

const getUserDoneAction = (user: User) => ({
    type: FETCH_USER_DONE,
    payload: user
});

export const getUserFailed = error => dispatch => {
    dispatch(getUserFailedAction(error));
};

const getUserFailedAction = error => ({
    type: FETCH_USER_FAILED,
    payload: error
});

/* USER POSTS ACTIONS */

export const getUserPosts = (userId: number) => (dispatch, getState) => {
    const { data } = getState().user;
    if (data == null || data.id !== userId) {
        dispatch(getUserPostsCanceledAction());
        dispatch(getUserPostsAction(userId));
    }
};

const getUserPostsAction = userId => ({
    type: FETCH_USER_POSTS_REQUESTED,
    payload: userId
});

export const getUserPostsCanceled = () => dispatch => {
    dispatch(getUserCanceledAction());
};

const getUserPostsCanceledAction = () => ({
    type: FETCH_USER_POSTS_CANCELED
});

export const getUserPostsDone = (posts: Post[]) => dispatch => {
    dispatch(getUserPostsDoneAction(posts));
};

const getUserPostsDoneAction = (posts: Post[]) => ({
    type: FETCH_USER_POSTS_DONE,
    payload: posts
});

export const getUserPostsFailed = error => dispatch => {
    dispatch(getUserPostsFailedAction(error));
};

const getUserPostsFailedAction = error => ({
    type: FETCH_USER_POSTS_FAILED,
    payload: error
});

export const removePost = (postId: number) => dispatch => {
    dispatch(removePostAction(postId));
}

const removePostAction = (postId: number) => ({
    type: REMOVE_USER_POST_REQUESTED,
    payload: postId
});

export const removePostDone = (postId: number) => (dispatch, getState) => {
    const { posts } = getState().user;
    dispatch(updateUserPostsAction(posts.items.filter(post => post.id !== postId)));
}

export const removePostFailed = (error) => dispatch => {
    dispatch(removePostFailedAction(error));
}

const removePostFailedAction = (error: string) => ({
    type: REMOVE_USER_POST_FAILED,
    payload: error
});

const updateUserPostsAction = (posts: Post[]) => ({
    type: UPDATE_USER_POSTS,
    payload: posts
});

export const loadMorePosts = (take: number = 10) => (dispatch, getState) => {
    const { posts } = getState().user;
    dispatch(loadMorePostsAction(posts.take + take));
}

const loadMorePostsAction = (take: number) => ({
    type: LOAD_MORE_POSTS,
    payload: take
})

export const closeNewPostModal = () => dispatch => {
    dispatch(changeNewPostModalVisibility(false));
}

export const showNewPostModal = () => dispatch => {
    dispatch(changeNewPostModalVisibility(true));
}

const changeNewPostModalVisibility = (visible: boolean) => ({
    type: CHANGE_NEW_POST_MODAL_VISIBILITY,
    payload: visible
});

export const pushPost = (title: string, body: string) => (dispatch, getState) => {
    const { data } = getState().user;
    dispatch(pushPostAction(data.id, title, body));
}

const pushPostAction = (userId: number, title: string, body: string) => ({
    type: PUSH_USER_POST_REQUESTED,
    payload: {
        userId: userId,
        title: title,
        body: body
    }
});

export const pushPostDone = (post: Post) => dispatch => {
    dispatch(pushPostDoneAction(post));
    dispatch(reset('newPostForm'));
}

const pushPostDoneAction = (post: Post) => ({
    type: PUSH_USER_POST_DONE,
    payload: post
});

export const pushPostFailed = (error) => dispatch => {
    dispatch(pushPostFailedAction(error));
}

const pushPostFailedAction = (error: string) => ({
    type: PUSH_USER_POST_FAILED,
    payload: error
});