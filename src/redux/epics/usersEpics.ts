import { fetchUsers } from '../../api';
import { mergeMap, catchError, map, takeUntil } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import { FETCH_USERS_REQUESTED, FETCH_USERS_CANCELED } from '../action-types';
import { getUsersDone, getUsersFailed } from '../actions';
import { of } from 'rxjs';

const getUsersEpic = action$ => action$.pipe(
    ofType(FETCH_USERS_REQUESTED),
    mergeMap(action => fetchUsers()
        .pipe(
            map(response => getUsersDone(response)),
            catchError(error => of(getUsersFailed(error))),
            takeUntil(action$.ofType(FETCH_USERS_CANCELED))
        )
    )
);

export const usersEpic = combineEpics(
    getUsersEpic
);