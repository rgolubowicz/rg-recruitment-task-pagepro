import { fetchUser, fetchPosts, deletePost, addPost } from '../../api';
import { ofType, combineEpics } from 'redux-observable';
import { mergeMap, map, catchError, takeUntil } from 'rxjs/operators';
import {
    FETCH_USER_CANCELED,
    FETCH_USER_REQUESTED,
    FETCH_USER_POSTS_REQUESTED,
    FETCH_USER_POSTS_CANCELED,
    REMOVE_USER_POST_REQUESTED,
    PUSH_USER_POST_REQUESTED
} from '../action-types';
import { of } from 'rxjs';
import {
    getUserDone,
    getUserFailed,
    getUserPostsDone,
    getUserPostsFailed,
    removePostFailed,
    removePostDone,
    pushPostDone,
    pushPostFailed,
} from '../actions';

const getUserEpic = action$ => action$.pipe(
    ofType(FETCH_USER_REQUESTED),
    mergeMap((action: any) => fetchUser(action.payload)
        .pipe(
            map(response => getUserDone(response)),
            catchError(error => of(getUserFailed(error.message))),
            takeUntil(action$.ofType(FETCH_USER_CANCELED))
        )
    )
)

const getUserPostsEpic = action$ => action$.pipe(
    ofType(FETCH_USER_POSTS_REQUESTED),
    mergeMap((action: any) => fetchPosts(action.payload)
        .pipe(
            map(response => getUserPostsDone(response)),
            catchError(error => of(getUserPostsFailed(error.message))),
            takeUntil(action$.ofType(FETCH_USER_POSTS_CANCELED))
        )
    )
)

const removeUserPostEpic = action$ => action$.pipe(
    ofType(REMOVE_USER_POST_REQUESTED),
    mergeMap((action: any) => deletePost(action.payload)
        .pipe(
            map(() => removePostDone(action.payload)),
            catchError(error => of(removePostFailed(error.message)))
        )
    )
)

const pushUserPostEpic = action$ => action$.pipe(
    ofType(PUSH_USER_POST_REQUESTED),
    mergeMap((action: any) => addPost(action.payload.userId, action.payload.title, action.payload.body)
        .pipe(
            map(response => pushPostDone(response)),
            catchError(error => of(pushPostFailed(error.message)))
        )
    )
)

export const userEpic = combineEpics(
    getUserEpic,
    getUserPostsEpic,
    removeUserPostEpic,
    pushUserPostEpic
);