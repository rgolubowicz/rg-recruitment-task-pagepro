import { mergeMap, catchError, map, takeUntil } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';
import {
    FETCH_POST_REQUESTED,
    FETCH_POST_CANCELED,
    FETCH_POST_COMMENTS_REQUESTED,
    FETCH_POST_COMMENTS_CANCELED,
    PUSH_POST_COMMENT_REQUESTED
} from '../action-types';
import { of } from 'rxjs';
import { fetchPost } from '../../api';
import { getPostDone, getPostFailed, getPostCommentsDone, getPostCommentsFailed, pushPostCommentDone, pushPostCommentFailed } from '../actions';
import { fetchComments, addComment } from '../../api/comments';

const getPostEpic = action$ => action$.pipe(
    ofType(FETCH_POST_REQUESTED),
    mergeMap((action: any) => fetchPost(action.payload)
        .pipe(
            map(response => getPostDone(response)),
            catchError(error => of(getPostFailed(error))),
            takeUntil(action$.ofType(FETCH_POST_CANCELED))
        )
    )
);

const getPostCommentsEpic = action$ => action$.pipe(
    ofType(FETCH_POST_COMMENTS_REQUESTED),
    mergeMap((action: any) => fetchComments(action.payload)
        .pipe(
            map(response => getPostCommentsDone(response)),
            catchError(error => of(getPostCommentsFailed(error))),
            takeUntil(action$.ofType(FETCH_POST_COMMENTS_CANCELED))
        )
    )
);

const pushPostCommentEpic = action$ => action$.pipe(
    ofType(PUSH_POST_COMMENT_REQUESTED),
    mergeMap((action: any) => addComment(action.payload.postId, action.payload.email, action.payload.title, action.payload.body)
        .pipe(
            map(response => pushPostCommentDone(response)),
            catchError(error => of(pushPostCommentFailed(error.message)))
        )
    )
)

export const postEpic = combineEpics(
    getPostEpic,
    getPostCommentsEpic,
    pushPostCommentEpic
);