import {
    FETCH_USERS_DONE,
    FETCH_USERS_REQUESTED,
    FETCH_USERS_FAILED
} from '../action-types';

const usersDefaultState = {
    items: [],
    loadedUsers: [],
    isFetching: false,
    error: null
};


export const usersReducer = (state = usersDefaultState, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case FETCH_USERS_REQUESTED: {
            return {
                ...state,
                isFetching: true
            };
        }
        case FETCH_USERS_DONE: {
            return {
                ...state,
                items: payload,
                isFetching: false,
                error: null
            };
        }
        case FETCH_USERS_FAILED: {
            return {
                ...state,
                items: [],
                isFetching: false,
                error: payload
            };
        }
        default: {
            return state;
        }
    }
}