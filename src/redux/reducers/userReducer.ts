import {
    FETCH_USER_REQUESTED,
    FETCH_USER_DONE,
    FETCH_USER_FAILED,
    FETCH_USER_POSTS_DONE,
    FETCH_USER_POSTS_REQUESTED,
    FETCH_USER_POSTS_FAILED,
    UPDATE_USER_POSTS,
    LOAD_MORE_POSTS,
    CHANGE_NEW_POST_MODAL_VISIBILITY,
    PUSH_USER_POST_REQUESTED,
    PUSH_USER_POST_DONE,
    PUSH_USER_POST_FAILED
} from '../action-types';

const userDefaultState = {
    data: null,
    isFetching: false,
    error: null,
    posts: {
        items: [],
        take: 5,
        isFetching: false,
        error: null
    },
    newPost: {
        modalVisible: false,
        adding: false,
        error: null
    }
};


export const userReducer = (state = userDefaultState, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case FETCH_USER_REQUESTED: {
            return {
                ...state,
                isFetching: true
            };
        }
        case FETCH_USER_DONE: {
            return {
                ...state,
                data: payload,
                isFetching: false,
                error: null
            };
        }
        case FETCH_USER_FAILED: {
            return {
                ...state,
                data: null,
                isFetching: false,
                error: payload,
            };
        }
        case FETCH_USER_POSTS_REQUESTED: {
            return {
                ...state,
                posts: {
                    ...userDefaultState.posts,
                    isFetching: true
                }
            };
        }
        case FETCH_USER_POSTS_DONE: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    items: payload,
                    isFetching: false
                }
            };
        }
        case FETCH_USER_POSTS_FAILED: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    items: [],
                    error: payload,
                    isFetching: false
                }
            };
        }
        case UPDATE_USER_POSTS: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    items: payload
                }
            };
        }
        case LOAD_MORE_POSTS: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    take: payload
                }
            };
        }
        case CHANGE_NEW_POST_MODAL_VISIBILITY: {
            return {
                ...state,
                newPost: {
                    ...state.newPost,
                    modalVisible: payload
                }
            };
        }
        case PUSH_USER_POST_REQUESTED: {
            return {
                ...state,
                newPost: {
                    ...state.newPost,
                    adding: true
                }
            };
        }
        case PUSH_USER_POST_DONE: {
            return {
                ...state,
                posts: {
                    ...state.posts,
                    items: [payload, ...state.posts.items]
                },
                newPost: {
                    ...state.newPost,
                    adding: false,
                    error: null,
                    modalVisible: false
                }
            };
        }
        case PUSH_USER_POST_FAILED: {
            return {
                ...state,
                newPost: {
                    ...state.newPost,
                    adding: false,
                    error: payload
                }
            };
        }
        default: {
            return state;
        }
    }
}