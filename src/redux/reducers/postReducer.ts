import {
    FETCH_POST_REQUESTED,
    FETCH_POST_DONE,
    FETCH_POST_FAILED,
    LOAD_MORE_POST_COMMENTS,
    FETCH_POST_COMMENTS_REQUESTED,
    FETCH_POST_COMMENTS_DONE,
    FETCH_POST_COMMENTS_FAILED,
    TOGGLE_POST_COMMENTS,
    PUSH_POST_COMMENT_DONE,
    PUSH_POST_COMMENT_REQUESTED,
    PUSH_POST_COMMENT_FAILED,
    CHANGE_NEW_COMMENT_MODAL_VISIBILITY,
    RESET_POST_COMMENTS
} from '../action-types';

const postDefaultState = {
    data: null,
    isFetching: false,
    error: null,
    comments: {
        items: [],
        take: 5,
        isFetching: false,
        error: null,
        visible: false
    },
    newComment: {
        modalVisible: false,
        adding: false,
        error: null
    }
};

export const postReducer = (state = postDefaultState, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case FETCH_POST_REQUESTED: {
            return {
                ...state,
                isFetching: true,
                comments: postDefaultState.comments
            };
        }
        case FETCH_POST_DONE: {
            return {
                ...state,
                data: payload,
                isFetching: false,
                error: null
            };
        }
        case FETCH_POST_FAILED: {
            return {
                ...state,
                data: null,
                isFetching: false,
                error: payload,
            };
        }
        case FETCH_POST_COMMENTS_REQUESTED: {
            return {
                ...state,
                comments: {
                    ...postDefaultState.comments,
                    isFetching: true,
                }
            };
        }
        case FETCH_POST_COMMENTS_DONE: {
            return {
                ...state,
                comments: {
                    ...state.comments,
                    items: payload,
                    isFetching: false,
                    error: null
                }
            };
        }
        case FETCH_POST_COMMENTS_FAILED: {
            return {
                ...state,
                comments: {
                    ...state.comments,
                    comments: [],
                    isFetching: false,
                    error: payload
                }
            };
        }
        case LOAD_MORE_POST_COMMENTS: {
            return {
                ...state,
                comments: {
                    ...state.comments,
                    take: payload
                }
            }
        }
        case TOGGLE_POST_COMMENTS: {
            return {
                ...state,
                comments: {
                    ...state.comments,
                    visible: payload
                }
            };
        }
        case CHANGE_NEW_COMMENT_MODAL_VISIBILITY: {
            return {
                ...state,
                newComment: {
                    ...state.newComment,
                    modalVisible: payload
                }
            };
        }
        case PUSH_POST_COMMENT_REQUESTED: {
            return {
                ...state,
                newComment: {
                    ...state.newComment,
                    adding: true
                }
            };
        }
        case PUSH_POST_COMMENT_DONE: {
            return {
                ...state,
                comments: {
                    ...state.comments,
                    items: [payload, ...state.comments.items]
                },
                newComment: {
                    ...state.newComment,
                    adding: false,
                    error: null,
                    modalVisible: false
                }
            };
        }
        case PUSH_POST_COMMENT_FAILED: {
            return {
                ...state,
                newComment: {
                    ...state.newComment,
                    adding: false,
                    error: payload
                }
            };
        }
        case RESET_POST_COMMENTS: {
            return {
                ...state,
                comments: postDefaultState.comments
            }
        }
        default:
            return state;
    }
}
