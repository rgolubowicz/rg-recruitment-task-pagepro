import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { usersReducer, userReducer, postReducer } from './reducers';
import { usersEpic, userEpic, postEpic } from './epics';
import { reducer as formReducer } from 'redux-form';

const w : any = window as any;
 
const epicMiddleware = createEpicMiddleware();

const composeEnhancers =
    process.env.NODE_ENV !== 'production' ?
    w.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose :
    compose


const store = createStore(
    combineReducers({
        users: usersReducer,
        user: userReducer,
        post: postReducer,
        form: formReducer
    }),
    composeEnhancers(
        applyMiddleware(
            thunk,
            epicMiddleware
        )
    )
);

epicMiddleware.run(combineEpics(
    usersEpic,
    userEpic,
    postEpic
))

export default store;