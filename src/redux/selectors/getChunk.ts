export const getChunk = (list, take, skip = 0) => {
    if (skip > list.length) {
        return [];
    }
    return list.slice(skip, take);
};