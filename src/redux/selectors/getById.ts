export const getById = (list, id: number) => {
    return list.find(obj => obj.id === id);
};