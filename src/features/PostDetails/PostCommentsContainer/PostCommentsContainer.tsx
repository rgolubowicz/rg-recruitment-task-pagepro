import { connect } from "react-redux";
import { CommentList } from "../../../components/CommentList";
import { getChunk } from "../../../redux/selectors/getChunk";
import {
  loadMorePostComments,
  togglePostComments,
  showNewCommentModal,
  closeNewCommentModal,
  pushPostComment
} from "../../../redux/actions";

const mapStateToProps = state => ({
  isFetching: state.post.comments.isFetching,
  error: state.post.comments.error,
  allItems: state.post.comments.items.length,
  comments: getChunk(state.post.comments.items, state.post.comments.take),
  take: state.post.comments.take,
  visible: state.post.comments.visible,
  newCommentModalVisible: state.post.newComment.modalVisible,
  newCommentError: state.post.newComment.error,
  newCommentSubmitting: state.post.newComment.adding
});

const mapDispatchToProps = {
  loadMorePostComments,
  toggleComments: togglePostComments,
  showNewCommentModal,
  closeNewCommentModal,
  pushPostComment
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentList);
