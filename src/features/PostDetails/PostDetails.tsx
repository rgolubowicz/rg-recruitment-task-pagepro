import React, { Component } from "react";
import { Post } from "../../models";
import { Loading } from "../../components";
import PostCommentsContainer from "./PostCommentsContainer/PostCommentsContainer";
import { Alert } from "react-bootstrap";

interface Props {
  isFetching: boolean;
  error: any;
  post: Post;
  commentsVisible: boolean;
  userLoaded: boolean;
  comments?: Comment[];
  match?: any;
  getPost: (postId: number) => void;
  getPostComments: (postId: number) => void;
}
export class PostDetails extends Component<Props> {
  componentWillMount = () => {
    const postId = +this.props.match.params.postId;
    this.props.getPost(postId);
  };
  render = () => {
    const { userLoaded, isFetching, error, post } = this.props;
    if (!userLoaded) {
      return null;
    }
    if (isFetching) {
      return (
        <div className="mt-5">
          <Loading variant="warning" text="Loading post details" />
        </div>
      );
    } else {
      return (
        <div className="mt-5">
          {error && (
            <Alert variant="danger">
              An error occurred while loading the post
            </Alert>
          )}
          {post && (
            <div>
              <h3 className="text-center font-weight-bold">{post.title}</h3>
              <p>{post.body}</p>
            </div>
          )}
          {!error && <PostCommentsContainer />}
        </div>
      );
    }
  };
}
