import { connect } from "react-redux";
import { PostDetails } from "./PostDetails";
import { getPost, getPostComments } from "../../redux/actions";

const mapStateToProps = state => ({
  post: state.post.data,
  isFetching: state.post.isFetching,
  error: state.post.error,
  userLoaded: state.user.data != null
});

const mapDispatchToProps = {
  getPost,
  getPostComments
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostDetails);
