import React, { Component } from "react";
import { Container, Alert } from "react-bootstrap";
import { User } from "../../../models";
import { Loading } from "../../../components";
import { FaArrowCircleLeft } from "react-icons/fa";
import UserPostsContainer from "../UserPostsContainer/UserPostsContainer";
import { Route } from "react-router-dom";
import PostDetailsContainer from "../../PostDetails/PostDetailsContainer";
import "./UserDetails.scss";

interface Props {
  isFetching: boolean;
  error: any;
  user: User;
  match?: any;
  history?: any;
  location?: any;
  getUser: (userId: number) => void;
  getUserPosts: (userId: number) => void;
}

export class UserDetails extends Component<Props> {
  componentWillMount = () => {
    const userId = +this.props.match.params.id;
    this.props.getUser(userId);
    this.props.getUserPosts(userId);
  };

  goBack = () => {
    const currentPath = this.props.location.pathname;
    const parentPath = currentPath.substring(0, currentPath.lastIndexOf("/"));
    this.props.history.push(parentPath);
  };

  render = () => {
    const { name } = { ...this.props.user };
    const { isFetching, match, error } = this.props;

    const backBlock = (
      <div className="back__button" onClick={this.goBack}>
        <h3 className="d-inline-block back__button--icon mb-0 c-pointer">
          <FaArrowCircleLeft />
        </h3>
        <h4 className="d-none d-md-inline-block ml-2 mb-0 c-pointer">Back</h4>
      </div>
    );

    return (
      <Container className="py-3">
        <div className="d-flex align-items-center justify-content-between">
          <div className="left__column">
            {backBlock}
          </div>
          <div className="center__column">
            {!isFetching && !error && (
              <h2 className="text-danger mb-0">{name}</h2>
            )}
            {isFetching && <Loading />}
          </div>
          <div className="right__column" />
        </div>
        {!isFetching && error && (
          <Alert className="my-3" variant="danger">
            An error occurred while loading the user
          </Alert>
        )}
        <Route exact path={`${match.url}`} component={UserPostsContainer} />
        <Route path={`${match.url}/:postId`} component={PostDetailsContainer} />
      </Container>
    );
  };
}
