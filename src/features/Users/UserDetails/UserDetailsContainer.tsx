import { UserDetails } from "./UserDetails";
import { connect } from "react-redux";
import { getUser, getUserPosts } from "../../../redux/actions";

const mapStateToProps = state => ({
  isFetching: state.user.isFetching,
  error: state.user.error,
  user: state.user.data
});

const mapDispatchToProps = {
  getUser,
  getUserPosts
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetails);
