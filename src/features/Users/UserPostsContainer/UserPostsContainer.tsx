import { connect } from "react-redux";
import {
  removePost,
  loadMorePosts,
  closeNewPostModal,
  showNewPostModal,
  pushPost
} from "../../../redux/actions";
import { PostList } from "../../../components/PostList";
import { getChunk } from "../../../redux/selectors/getChunk";

const mapStateToProps = state => ({
  isFetching: state.user.posts.isFetching,
  error: state.user.posts.error,
  allItems: state.user.posts.items.length,
  posts: getChunk(state.user.posts.items, state.user.posts.take),
  userLoaded: state.user.data != null,
  take: state.user.posts.take,
  newPostModalVisible: state.user.newPost.modalVisible,
  newPostError: state.user.newPost.error,
  newPostSubmitting: state.user.newPost.adding
});

const mapDispatchToProps = {
  removePost,
  loadMorePosts,
  showNewPostModal,
  closeNewPostModal,
  pushPost
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostList);
