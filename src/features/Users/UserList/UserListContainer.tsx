import { UserList } from "./UserList";
import { connect } from "react-redux";
import { getUsers } from "../../../redux/actions";

const mapStateToProps = state => ({
  isFetching: state.users.isFetching,
  error: state.users.error,
  users: state.users.items
});

const mapDispatchToProps = {
  getUsers
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
