import React, { Component } from "react";
import { UserCard } from "../../../components/UserCard/UserCard";
import { Container, Row, Col } from "react-bootstrap";
import { Loading } from "../../../components";
import { User } from "../../../models";

interface Props {
  match: any;
  users: User[];
  isFetching: boolean;
  getUsers: () => void;
}

export class UserList extends Component<Props> {
  componentDidMount = () => {
    this.props.getUsers();
  };

  render = () => {
    const { isFetching, users } = this.props;
    return (
      <Container className="py-3">
        <h2 className="text-center text-danger">Users</h2>
        <Row className="justify-content-center">
          {users.map(user => (
            <Col
              xs={12}
              sm={6}
              md={4}
              lg={3}
              className="p-3"
              key={user.id.toString()}
            >
              <UserCard {...user} />
            </Col>
          ))}
        </Row>

        {isFetching && (
          <Row>
            <Col>
              <Loading text="Loading users" />
            </Col>
          </Row>
        )}
      </Container>
    );
  };
}
