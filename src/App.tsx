import React from 'react';
import './App.scss';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';
import Users from './features/Users/Users';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route
          path='/'
          component={Users}
        />
        {/* <Route
          exact
          path='/setup'
          component={Setup}
        />
        <Route
          exact
          path='/game'
          component={Game}
        /> */}
      </Switch>
    </BrowserRouter>
  )
}

export default App;
