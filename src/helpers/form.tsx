import React from "react";

export const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div className="form-group">
    <label htmlFor={input.name}>{label}</label>
    <input
      className="form-control"
      id={input.name}
      {...input}
      placeholder={label}
      type={type}
    />
    <div>
      {touched &&
        ((error && <span className="error-message">{error}</span>) ||
          (warning && (
            <span className="error-message text-warning">{warning}</span>
          )))}
    </div>
  </div>
);

export const renderTextarea = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div className="form-group">
    <label htmlFor={input.name}>{label}</label>
    <textarea
      className="form-control"
      id={input.name}
      {...input}
      placeholder={label}
      type={type}
    />
    <div>
      {touched &&
        ((error && <span className="error-message">{error}</span>) ||
          (warning && (
            <span className="error-message text-warning">{warning}</span>
          )))}
    </div>
  </div>
);

export const required = value =>
  value || typeof value === "number" ? undefined : "Field is required";

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
