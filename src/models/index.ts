export * from './Address.interface';
export * from './Company.interface';
export * from './User.interface';
export * from './Post.interface';
export * from './Comment.interface';