import { Address, Company } from '.';

export interface User {
    id: number;
    name: string;
    email: string;
    address?: Address;
    phone: string;
    website: string;
    company?: Company;
}