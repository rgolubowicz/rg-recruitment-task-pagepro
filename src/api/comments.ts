import { Comment } from '../models';
import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { COMMENTS_URL } from './endpoints';


export const fetchComments = (postId?: number) => {
    const url = COMMENTS_URL + (postId ? `?postId=${postId}` : '');
    return ajax.getJSON<Comment[]>(url);
};


export const fetchComment = (commentId: number) => (
    ajax.getJSON<Comment>(`${COMMENTS_URL}/${commentId}`)
)

export const deleteComment = (commentId: number) => (
    ajax.delete(`${COMMENTS_URL}/${commentId}`)
)

export const addComment = (postId: number, email: string, name: string, body: string): Observable<Comment> => {
    const requestBody = {
        postId,
        email,
        name,
        body
    };
    return ajax.post(COMMENTS_URL, requestBody)
        .pipe(
            map(response => response.response)
        );
}