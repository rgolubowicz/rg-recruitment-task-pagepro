import { User } from '../models';
import { ajax } from 'rxjs/ajax';
import { USERS_URL } from './endpoints';


export const fetchUsers = () => (
    ajax.getJSON<User[]>(USERS_URL)
);

export const fetchUser = userId => (
    ajax.getJSON<User>(`${USERS_URL}/${userId}`)
);
