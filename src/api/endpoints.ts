export const COMMENTS_URL = `${process.env.REACT_APP_API_URL}/comments`;
export const POSTS_URL = `${process.env.REACT_APP_API_URL}/posts`;
export const USERS_URL = `${process.env.REACT_APP_API_URL}/users`;