import { Post } from '../models';
import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { POSTS_URL } from './endpoints';


export const fetchPosts = (userId?: number) => {
    const url = POSTS_URL + (userId ? `?userId=${userId}` : '');
    return ajax.getJSON<Post[]>(url);
};


export const fetchPost = postId => (
    ajax.getJSON<Post>(`${POSTS_URL}/${postId}`)
)

export const deletePost = postId => (
    ajax.delete(`${POSTS_URL}/${postId}`)
)

export const addPost = (userId: number, title: string, body: string): Observable<Post> => {
    const requestBody = {
        userId,
        title,
        body
    };
    return ajax.post(POSTS_URL, requestBody)
        .pipe(
            map(response => response.response)
        );
}